@extends('layouts.master')

@section('page')
	Add New Product
@endsection

@section('content')
 
<!-- Basic Form-->
<div class="col-lg-10">
  <div class="card">
	<div class="card-header d-flex align-items-center">
	  <h3 class="h4">Add Product Form</h3>
	</div>
	<div class="card-body">
	 
	  <form action="{{ route('products.store') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
		<div class="form-group">
		  <label class="form-control-label">Product Name</label>
		  <input type="text" name="name" class="form-control" value="{{ old('name') }}">
		</div>
		<div class="form-group">       
		  <label class="form-control-label">Price</label>
		  <input type="number" name="price" class="form-control" value="{{ old('price') }}">
		</div>
		<div class="form-group">       
		  <label class="form-control-label">Product Image</label>
		  <input type="file" name="image" class="form-control">
		</div>
		<div class="form-group">
		  <label for="description">Description</label>
		  <textarea name="description" id="description" cols="10" rows="5" class="form-control">{{ old('description') }}</textarea>
		</div>
		<div class="form-group">       
		  <input type="submit" value="Add Product" class="btn btn-primary">
		</div>
	  </form>
	</div>
  </div>
</div>  
@endsection




