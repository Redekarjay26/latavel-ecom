@extends('layouts.master')

@section('page')
	Edit Product
@endsection

@section('content')
 
<!-- Basic Form-->
<div class="col-lg-10">
  <div class="card">
	<div class="card-header d-flex align-items-center">
	  <h3 class="h4">Edit Product</h3>
	</div>
	<div class="card-body">
	 
	  <form action="{{ route('products.update', ['id' => $product->id ]) }}" method="post">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
		<div class="form-group">
			  <label for="name">Name</label>
			  <input type="text" name="name" value="{{ $product->name }}" class="form-control">
		</div>
		<div class="form-group">
			  <label for="image">Price</label>
			  <input type="number" name="price" value="{{ $product->price }}" class="form-control">
		</div>
		<div class="form-group">
			  <label for="image">Image</label>
			  <input type="file" name="image" class="form-control">
		</div>
		<div class="form-group">
			  <label for="description">Description</label>
			  <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ $product->description }}</textarea>
		</div>
		<div class="form-group">       
		  <input type="submit" value="Update Product" class="btn btn-primary">
		</div>
	  </form>
	</div>
  </div>
</div>  
@endsection




