@extends('layouts.master')


@section('content')

<div class="card">
    <div class="card-header d-flex align-items-center">
	  <h3 class="h4">Product</h3>
	</div>

     <div class="card-body">
		<table class="table table-bordered">
		<thead>
			  <th>Name</th>
			  <th>Price</th>
			  <th>Image</th>
			  <th>Edit</th>
			  <th>Delete</th>
		</thead>
		<tbody>
			@foreach($products as $product)
			<tr>
			  <td>{{ $product->name }}</td>
			  <td>{{ $product->price }}</td>
			   <td><img src="{{$product->image}}" width="50px" height="50px"></td>
			  <td>
					<a href="{{ route('products.edit', ['id' => $product->id ]) }}" class="btn btn-default btn-xs">Edit</a>
			  </td>
			  <td>
					<form action="{{ route('products.destroy', ['id' => $product->id ]) }}" method="post">
						  
						  {{ method_field('DELETE')}}
						  {{ csrf_field() }}
						  <button class="btn btn-xs btn-danger">Delete</button>
					</form>
			  </td>
		</tr>
		@endforeach
		</tbody>
	</table>
    </div>
</div>

@endsection