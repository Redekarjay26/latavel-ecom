
	@include('includes.header')
    <div class="page">
    @include('includes.nav')
	
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        @include('includes.sitebar')
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">@yield('page')</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
          <section class="dashboard-counts no-padding-bottom">
            <div class="container-fluid">
              @yield('content')
            </div>
        </div>
          </section>


     @include('includes.footer')