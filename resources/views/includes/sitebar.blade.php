<nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar"><img src="{{asset('img/avatar-1.jpg')}}" alt="..." class="img-fluid rounded-circle"></div>
            <div class="title">
              <h1 class="h4">{{ Auth::user()->name }}</h1>
              
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
		  @if(Auth::user()->role_id == 1)
          <ul class="list-unstyled">
            <li><a href="{{ route('Dashboard') }}"> <i class="icon-home"></i>Home </a></li>
			
            <li><a href="{{ route('products.index') }}"> <i class="icon-grid"></i>Products </a></li>
			
            <li><a href="{{ route('products.create') }}"> <i class="fa fa-bar-chart"></i>Add Product </a></li>
			
            
            <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Example dropdown </a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="#">Page</a></li>
                <li><a href="#">Page</a></li>
                <li><a href="#">Page</a></li>
              </ul>
            </li>
            
          </ul>
		  
		  
		  @endif
        </nav>