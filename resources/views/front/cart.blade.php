@extends('layouts.frontend')



@section('content')


<!-- Page Content -->
<div class="container">

    <h2 class="mt-5"><i class="fa fa-shopping-cart"></i> Shooping Cart</h2>
    <hr>
    @if ( session()->has('msg') )
        <div class="alert alert-success">{{ session()->get('msg') }}</div>
    @endif

    @if(Cart::count() > 0 )

    <h4 class="mt-5">{{ Cart::count() }} items(s) in Shopping Cart</h4>

    <div class="cart-items">
        
        <div class="row">
            
            <div class="col-md-12">
                
                <table class="table">
                    
                    <tbody>

                       @foreach (Cart::instance('default')->content() as $item )
                        
                        <tr>
                            <td><img src="{{ $item->model->image }}" style="width: 5em"></td>
                            <td>
                                <strong>{{ $item->model->name }}</strong><br> {{ $item->model->description }}
                            </td>
                            
                            <td>
        
                                <form action="{{ route('cart.destroy', $item->rowId) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-link btn-link-dark">Remove</button>
                                </form><br>
                                <!--<a href="">Save for later</a> -->

                            </td>
                            
                            <td>
                                    <select class="form-control quantity" style="width: 4.7em" data-id="{{ $item->rowId }}">
                                       @for ($i = 1; $i < 5 + 1; $i++)
                                        <option {{ $item->qty == $i ? 'selected' : '' }}>{{$i}}</option>
                                      @endfor

                                    </select>
                                </td>
                            
                            <td>${{ $item->model->price }}</td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>

            </div>   
            <!-- Price Details -->
                <div class="col-md-6">
                        <div class="sub-total">
                             <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="2">Price Details</th>
                                    </tr>
                                </thead>
                                    <tr>
                                        <td>Subtotal </td>
                                        <td>${{ Cart::subtotal() }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <td>${{ Cart::tax() }}</td>
                                    </tr>
                                    <tr>
                                        <th>Total</th>
                                        <th>${{ Cart::total() }}</th>
                                    </tr>
                             </table>           
                         </div>
                    </div>
                <!-- Save for later  -->
                <div class="col-md-12">
                    <a href="{{route('index')}}" class="btn btn-outline-dark">Continue Shopping</a>
                    <a href="{{route('checkout')}}" class="btn btn-outline-info">Proceed to checkout</a>
                <hr>

                </div>
                @else
                    <h5> No items in cart</h5>
                @endif

                <!--<div class="col-md-12">
                
                <h4>2 items Save for Later</h4>
                <table class="table">
                    
                    <tbody>
                        
                        <tr>
                            <td><img src="" style="width: 5em"></td>
                            <td>
                                <strong>Mac</strong><br> This is some text for the product
                            </td>
                            
                            <td>
        
                                <a href="">Remove</a><br>
                                <a href="">Save for later</a>

                            </td>
                            
                            <td>
                                <select name="" id="" class="form-control" style="width: 4.7em">
                                    <option value="">1</option>
                                    <option value="">2</option>
                                </select>
                            </td>
                            
                            <td>$233</td>
                        </tr>

                        <tr>
                            <td><img src="images/01.jpg" style="width: 5em"></td>
                            <td>
                                <strong>Laptop</strong><br> This is some text for the product
                            </td>
                            
                            <td>
        
                                <a href="">Remove</a><br>
                                <a href="">Save for later</a>

                            </td>
                            
                            <td>
                                <select name="" id="" class="form-control" style="width: 4.7em">
                                    <option value="">1</option>
                                    <option value="">2</option>
                                </select>
                            </td>
                            
                            <td>$233</td>
                        </tr>

                        <tr>
                            <td><img src="images/12.jpg" style="width: 5em"></td>
                            <td>
                                <strong>Laptop</strong><br> This is some text for the product
                            </td>
                            
                            <td>
        
                                <a href="">Remove</a><br>
                                <a href="">Save for later</a>

                            </td>
                            
                            <td>
                                <select name="" id="" class="form-control" style="width: 4.7em">
                                    <option value="">1</option>
                                    <option value="">2</option>
                                </select>
                            </td>
                            
                            <td>$233</td>
                        </tr>

                    </tbody>

                </table>

            </div>  
                </div>


            </div>
        </div>
 /.container -->

 <script type="text/javascript">
     window.setTimeout(function () {
    $(".alert-success").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
    });
}, 2000);

 </script>

@endsection