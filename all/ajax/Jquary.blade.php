<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

  <div class="flex-md-row">
    <h2 class="text-center">Ajax Crude</h2>
    <!-- Button to Open the Modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Student Data </button>
<hr>
    <table class="table table-dark">
    <thead>
      <tr>
        <th>#</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Course</th>
        <th>Section</th>
        <th>Image</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($students as $student)
      <tr>
        <td>{{$student->id}}</td>
        <td>{{$student->fname}}</td>
        <td>{{$student->lname}}</td>
        <td>{{$student->course}}</td>
        <td>{{$student->section}}</td>
        <td><img src="{{ asset('img/'.$student->image) }}" alt="" width="30" height="30"></td>
        <td>
          <a href="#" class="btn btn-warning edit">Edit</a>
          <a href="#" class="btn btn-success delete">Delete</a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>


      <!-- The Modal -->
    <div class="modal" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Add Data</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
            <form id="addform" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="email">F Name:</label>
                <input type="text" class="form-control" name="fname">
              </div>
              <div class="form-group">
                <label for="lname">L Name:</label>
                <input type="text" class="form-control" name="lname">
              </div>
              <div class="form-group">
                <label for="course">Course:</label>
                <input type="text" class="form-control" name="course">
              </div>
              <div class="form-group">
                <label for="section">Section:</label>
                <input type="text" class="form-control" name="section">
              </div>
              <div class="form-group">
                <label for="image">Image:</label>
                <input type="file" class="form-control" name="image">
              </div> 
              <button type="submit" class="btn btn-primary">Submit</button>
          </form>
          </div>
        </div>
      </div>
    </div>
     
<!-- The Modal -->
    <div class="modal" id="editModal">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Edit Data</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
            <form id="editform" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden" name="id" id="id">
              <div class="form-group">
                <label for="email">F Name:</label>
                <input type="text" class="form-control" name="fname" id="fname">
              </div>
              <div class="form-group">
                <label for="lname">L Name:</label>
                <input type="text" class="form-control" name="lname" id="lname">
              </div>
              <div class="form-group">
                <label for="course">Course:</label>
                <input type="text" class="form-control" name="course" id="course">
              </div>
              <div class="form-group">
                <label for="section">Section:</label>
                <input type="text" class="form-control" name="section" id="section">
              </div>
              <div class="form-group">
                <label for="image">Image:</label>
                <input type="file" class="form-control" name="image" id="image">
              </div> 
              <button type="submit" class="btn btn-primary">Submit</button>
          </form>
          </div>
        </div>
      </div>
    </div>


<!-- The Modal -->
    <div class="modal" id="deleteModal">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Delete Student</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
            <form id="deleteform" method="GET" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden" name="id" id="delete_id">
              <P>Are you sure! you want to detete data</P>
              <button type="submit" class="btn btn-primary">Delete Data</button>
          </form>
          </div>
        </div>
      </div>
    </div>

  </div>      
</div>

<script type="text/javascript">

    $(document).ready(function(){ 
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
  });


$('.delete').on('click', function(){
    $('#deleteModal').modal('show');

    $tr = $(this).closest('tr');

    var data = $tr.children('td').map(function(){
      return $(this).text();
    }).get();

    $('#delete_id').val(data[0]);
  });

$('#deleteform').on('submit', function(e){
    e.preventDefault();

    var id = $('#delete_id').val();
    var request = new FormData(this);

    $.ajax({

      url: "/jquarydelete/"+id,
        method:"GET",
        data:request,
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){ 
            $('#deleteModal').modal('hide');
            alert('Data deleted');
            location.reload();
    
        }

    });

  });


 $(document).ready(function(){

  $('.edit').on('click', function(){
    $('#editModal').modal('show');

    $tr = $(this).closest('tr');

    var data = $tr.children('td').map(function(){
      return $(this).text();
    }).get();

    $('#id').val(data[0]);
    $('#fname').val(data[1]);
    $('#lname').val(data[2]);
    $('#course').val(data[3]);
    $('#section').val(data[4]);
    
  });

  $('#editform').on('submit', function(e){
    e.preventDefault();

    var id = $('#id').val();
    var request = new FormData(this);

    $.ajax({

      url: "/jquaryupdate/"+id,
        method:"POST",
        data:request,
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){ 
            $('#editModal').modal('hide');
            alert('Data updated');
            location.reload();
    
        }

    });

  });


 });



  $(document).ready(function(){

    $('#addform').on('submit', function(e){
      e.preventDefault();

      var request = new FormData(this);

      $.ajax({
        url: "/jquarystore",
        method:"POST",
        data:request,
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){ 
            $('#myModal').click();
            $('#addform')[0].reset();
            alert('Data has been submit');
            location.reload();
    
        }
      });

    });
      
  });
  
</script>











</body>
</html>