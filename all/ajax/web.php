<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/jquary', 'JquaryController@index')->name('jquary.index');
Route::post('/jquarystore', 'JquaryController@store')->name('jquarystore');
Route::post('/jquaryupdate/{id}', 'JquaryController@update')->name('jquaryupdate');
Route::get('/jquarydelete/{id}', 'JquaryController@destroy')->name('jquarydelete');







Route::get('/products', 'ProductsController@index')->name('product.index');
Route::get('/products/create', 'ProductsController@create')->name('product.create');
Route::post('/products/store', 'ProductsController@store')->name('product.store');
Route::get('/products/edit/{id}', 'ProductsController@edit')->name('product.edit');
Route::post('/products/update/{id}', 'ProductsController@update')->name('product.update');
Route::get('/products/delete/{id}', 'ProductsController@destroy')->name('product.delete');
/*
Route::group(['middleware' => ['auth', 'admin' ]], function(){

    Route::get('dashboard', function() {
    	return view('admin.dashboard');
    });
});*/


