<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Product;

class CartController extends Controller
{
    public function index(){

    	return view('front.cart');
    }


    public function store(Request $request) {

        $dubl = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id === $request->id;
        });

        if ($dubl->isNotEmpty()) {
            return redirect('cart')->with('msg','Item is already in your cart');
        }

        Cart::add($request->id, $request->name, 1, $request->price)->associate('App\Product');

        return redirect('cart')->with('msg','Item has been added to cart');

    }

    public function destroy($id) {

        Cart::remove($id);

        return redirect()->back()->with('msg','Item has been removed from cart');

    }

}
