<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [

	'uses' =>'FrontEndController@index',
	'as' =>'index'

]);

Route::get('/cart', [

	'uses' =>'CartController@index',
	'as' =>'cart'

]);


Route::post('/cart','CartController@store')->name('cart_add');

Route::delete('/cart/remove/{product}','CartController@destroy')->name('cart.destroy');

// Checkout
Route::get('/checkout','CheckoutController@index')->name('checkout');
Route::post('/checkout','CheckoutController@store')->name('checkout_pay');


Auth::routes();

Route::get('/Dashboard', 'HomeController@index')->name('Dashboard');

Route::resource('products', 'ProductsController');




Route::get('empty', function() {
    Cart::instance('default')->destroy();
});
