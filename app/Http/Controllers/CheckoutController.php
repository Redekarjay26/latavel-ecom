<?php

namespace App\Http\Controllers;

use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class CheckoutController extends Controller
{
    public function index(){

    	return view('front.checkout');
    }


    public function store(Request $request) {

    	//dd($request->all());
    	try 
    	{

            Stripe::charges()->create([
                'amount' => Cart::total(),
                'currency' => 'USD',
                'source' => $request->stripeToken,
                'description' => 'Some Text',
                
                
            ]);

            Cart::instance('default')->destory();
            
            return redirect()->back()->with('msg','Success Thank you');

    	}catch (Exception $e) {
            // Code
        }


    }
}
