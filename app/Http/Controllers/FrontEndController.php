<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class FrontEndController extends Controller
{
    public function index(){

    	$product = Product::paginate(5);

    	return view('index')->with('products', $product);
    }
}
